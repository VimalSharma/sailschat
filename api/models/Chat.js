/**
* Chat.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {

		fromuser:{
			model :'User',
			required:true
		},
		touser:{
			model :'User',
			required:true
		},
  		message:{
  			type:'string',
  			required:true
  		},
  		date:{
  			type:'date',
  			defaultsTo:Date.now()
  		}
  		
	}
};

