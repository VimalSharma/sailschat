/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
	register:function (req,res) {
		
		var data = req.body;

        if (_.isEmpty(data)) {
            return res.badRequest({
                'Error': "Empty Body"
            });
        }

		User.create(data, function(error, created) {
	        if (error) {
	            return res.badRequest({
	                error: "Bad Request"
	            });
	        }
	        if (created) {
	            return res.ok({
	                "success": created
	            });
	        }
        })
	},

	login:function (req,res) {
		var data = req.body;

        if (_.isEmpty(data)) {
            return res.badRequest({
                'Error': "Empty Body"
            });
        }

        User.findOne({
        	$or: [{
                $and: [{
                    name: data.user
                }, {
                    password: data.password
                }]
            }, {
                $and: [{
                    email: data.user
                }, {
                    password: data.password
                }]
            }]
            }).exec(function(err, success) {
            if (err) {
                return res.badRequest({
                    error: "Bad Request"
                });
            }
            if (typeof success !== undefined) {
                if (_.isEmpty(success)) {
                    return res.badRequest({
                        error: "Empty Success"
                    });
                } else {
                	var id = success.id;
                	var obj = {};
                	obj.status = true;
                	User.update({id}, obj, function(error, updated) {
                		if (error) {
                            return res.badRequest({
                                error: error
                            });
                        }
                        if (updated) {
                        	req.session.loginUser = updated[0];		                   
		                    return res.ok({
		                        "data": updated[0]
		                    });
                        }
                    });
                }
            }
        });
    },

	logout:function (req,res) {
		if(req.session && req.session.loginUser){
			var id = req.session.loginUser.id;
			//var id = "572db9f57e82bdb01f0d9cf0";
        	var obj = {};
        	obj.status = false;
        	User.update({id}, obj, function(error, updated) {
        		if (error) {
                    return res.badRequest({
                        error: error
                    });
                }
                if (updated) {
                	req.session.loginUser = updated[0];
                    console.log("===logout=====", req.session.loginUser);
                    return res.ok({
                        "data": updated[0]
                    });
                }
            });
		}
	},

	getActiveUsers: function (req,res) {
		User.find({"status": true}, function(err, found) {
			return res.ok({
                "data": found
            });
		});
	},

	bindUser: function (req,res) {
		if(req.session && req.session.loginUser){
			var id = req.session.loginUser.id;
			return res.ok({
                "data": req.session.loginUser
            });
		}
	},
	
};

