﻿angular.module('socketApp')
  .config(function ($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('home', {
            url: '/',
            templateUrl: '../template/register.html'
        }).state('login', {
            url: '/login',
            templateUrl: '../template/login.html'
        }).state('register', {
            url: '/register',
            templateUrl: '../template/register.html'
        }).state('chat', {
            url: '/chat',
            templateUrl: '../template/chat.html'
        }).state('users', {
            url: '/users',
            templateUrl: '../template/userlist.html'
        }).state('mychat', {
            url: '/mychat',
            templateUrl: '../template/mychat.html'
        });
      $urlRouterProvider.otherwise('/');
  });