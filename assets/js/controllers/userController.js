﻿socketApp.controller('UserController', ['$http', '$log', '$scope','$state', function ($http, $log, $scope,$state) {
    $scope.register=function()
    {
        var url = 'http://localhost:3000/register';       
        var data = { 'name': $scope.username, 'email': $scope.email, 'password': $scope.password };     
        $http.post(url, data)
           .success(function (data, status, headers, config) {
               if(data && data.success)
               {
                   $state.go('login');
               }                       
           })
           .error(function (data, status, header, config) {
             
           });
    }

    $scope.login = function () {
        var url = 'http://localhost:3000/login';
        var data = { 'user': $scope.username, 'password': $scope.password };
        $http.post(url, data)
           .success(function (res, status, headers, config) {
               if (res.data) {
                   $state.go('users');
               }          
           })
           .error(function (data, status, header, config) {
               console.log(data);
           });
    }


    $scope.redirecttologin=function()
    {
        $state.go('login');
    }

    $scope.getActiveUsers = function () {
      $http.get('http://localhost:3000/users')
        .success(function (success) {
             $scope.userList = success.data;
             $log.info(success);
             })
        .error(function (data, status, header, config) {
           console.log(data);
         });
    };

}]);