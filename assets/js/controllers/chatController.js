﻿socketApp.controller('ChatController', ['$http', '$log', '$scope', function ($http, $log, $scope) {


    $scope.predicate = '-id';
    $scope.reverse = false;
    $scope.baseUrl = 'http://localhost:3000';
    $scope.chatList = [];
    $scope.getAllchat = function () {

        io.socket.get('/chat/addconv');

        $http.get($scope.baseUrl + '/chat')
             .success(function (success_data) {

                 $scope.chatList = success_data;
                 $log.info(success_data);
             });
    };

    $scope.getAllchat();
    $scope.chatUser = ""
    $scope.chatMessage = "";

    io.socket.on('chat', function (obj) {

        if (obj.verb === 'created') {
            $log.info(obj)
            $scope.chatList.push(obj.data);
            $scope.$digest();
        }

    });

    $scope.sendMsg = function () {
        $log.info($scope.chatMessage);
        io.socket.post('/chat/addconv/', { user: $scope.chatUser, message: $scope.chatMessage });
        $scope.chatMessage = "";
    };

    $scope.bindUser = function () {
      $http.get('http://localhost:3000/user')
        .success(function (success) {
          console.log("success", success.data.name);
             $scope.chatUser = success.data.name;
             $log.info(success);
             })
        .error(function (data, status, header, config) {
           console.log(data);
         });
    };

}]);